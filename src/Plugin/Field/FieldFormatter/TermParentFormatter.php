<?php

namespace Drupal\term_parent_formatter\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;

/**
 * Plugin implementation of the 'term parent' formatter.
 *
 * @FieldFormatter(
 *   id = "term_parent",
 *   label = @Translation("Term with Parents"),
 *   description = @Translation("Display the term and its parents"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class TermParentFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\taxonomy\Entity\Term $entity */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $labels = [];
      foreach ($entity->getParents() as $parent) {
        $labels[] = Html::escape($parent->label());
      }
      $labels[] = $entity->label();
      $elements[$delta] = [
        '#markup' => implode(' &raquo; ', $labels),
      ];
    }

    return $elements;
  }

}
